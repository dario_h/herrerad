//
//  TableViewCellLabel.swift
//  herreraD-Examen
//
//  Created by Dario Herrera on 6/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import UIKit

class TableViewCellLabel: UITableViewCell {

    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var labelLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillDat(datos: dataApi) {
        labelLabel.text = datos.label
        labelLabel.text = "\(datos.value)"
    }

}
