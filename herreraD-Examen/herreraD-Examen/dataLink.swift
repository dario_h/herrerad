//
//  dataLink.swift
//  herreraD-Examen
//
//  Created by Dario Herrera on 5/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import Foundation

struct ApiOneInfo: Decodable {
    
    //json campo weather
    //let weather: [DarioWeather]
    
    let viewTitle: String
    let date: String
    let nextLink: String
    
    
}


struct DarioWeather: Decodable {
    //obtengo los objetos del arreglo
    //let id: Int
    //let nextLink: String
}


struct ApiDosInfo: Decodable {
    
    //json campo weather
    var data: [dataApi]
    
    
}

struct dataApi: Decodable {
    //obtengo los objetos del arreglo
    let label: String
    let value: Int
}



