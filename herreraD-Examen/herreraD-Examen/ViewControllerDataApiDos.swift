//
//  ViewControllerDataApiDos.swift
//  herreraD-Examen
//
//  Created by Dario Herrera on 5/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import UIKit

class ViewControllerDataApiDos: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var nombre : String = ""
    var data:[dataApi] = []
    var dataIndex = 0
    
    
    var arrayofItems: [String] = ["","","",""]
    var numbers: [Int] = [0,0,0,0]
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var cargarTableView: UITableView!
    
    @IBOutlet weak var txtVerNombre: UILabel!
    
    @IBOutlet weak var miNombre: UILabel!
    
    @IBOutlet weak var promedio: UILabel!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtVerNombre?.text = "Hi "+nombre
        
        let urlString = "https://api.myjson.com/bins/182sje"
        let url = URL (string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else{
                
                print("error no data")
                return
            }
            
            guard let linkInfo = try? JSONDecoder().decode(ApiDosInfo.self, from: data)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            
            DispatchQueue.main.async{
                self.arrayofItems[0] = "\(linkInfo.data[0].label)"
                self.arrayofItems[1] = "\(linkInfo.data[1].label)"
                self.arrayofItems[2] = "\(linkInfo.data[2].label)"
                self.arrayofItems[3] = "\(linkInfo.data[3].label)"
                
                
                self.numbers[0] = linkInfo.data[0].value
                self.numbers[1] = linkInfo.data[1].value
                self.numbers[2] = linkInfo.data[2].value
                self.numbers[3] = linkInfo.data[3].value
                
                
                
                
                let val1: Int = linkInfo.data[0].value
                let val2: Int = linkInfo.data[1].value
                let val3: Int = linkInfo.data[2].value
                let val4: Int = linkInfo.data[3].value
                
                
                
                self.cargarTableView.reloadData()
                
                let suma : Int = val1+val2+val3+val4
                
                let cte : Int = 4
                
                let promedio: Double = Double(suma/cte)
                
                self.promedio.text = "\(promedio)"
                
                
                
            }
            
            
            
        }
        task.resume()
        
        
    }

    func  numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        dataIndex = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayofItems.count
    }
    
    //metodo que devuelve data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! TableViewCellLabel
        cell.labelLabel.text = arrayofItems[indexPath.row]
        cell.labelValue.text = "\(numbers[indexPath.row])"
        return cell
    
       
    }
    
    
    
    
    
    /*func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "section 1"
        default:
            return "section 2"
        }
    }*/

}
