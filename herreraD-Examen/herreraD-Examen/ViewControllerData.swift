//
//  ViewControllerData.swift
//  herreraD-Examen
//
//  Created by Dario Herrera on 5/6/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import UIKit

class ViewControllerData: UIViewController {
    
    
    
    @IBOutlet weak var txtVerData: UILabel!
    
    @IBOutlet weak var txtMateria: UILabel!
    @IBOutlet weak var txtViewTittle: UILabel!
    
    @IBOutlet weak var ingresarNombre: UITextField!
    
    @IBOutlet weak var textFieldNombre: UITextField!
    
    
    @IBAction func btnSiguiente(_ sender: Any) {
        performSegue(withIdentifier: "pasarDatos", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pasarDatos" {
            let segundaVista = segue.destination as? ViewControllerDataApiDos
            segundaVista?.nombre = textFieldNombre.text!
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldNombre.becomeFirstResponder()
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL (string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else{
                
                print("error no data")
                return
            }
            
            guard let linkInfo = try? JSONDecoder().decode(ApiOneInfo.self, from: data)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            
            DispatchQueue.main.async{
               
                self.txtViewTittle.text = "\(linkInfo.viewTitle)"
                self.txtMateria.text = "\(linkInfo.date)"
                
            }
        }
        task.resume()

    
    }
    
 
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
